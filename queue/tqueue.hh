// a templated queue that should work with all kinds of executors
// (as long as the executors on both ends are synchronization compatible)
/*
 * Copyright (c) 1998-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#pragma once
#ifndef TQUEUE_HH_INCLUDED
#define TQUEUE_HH_INCLUDED

#include "ring.hh"

#include <mutex> // for unique_lock


struct TQueueBase
{
    enum Status
    {
        success = 0,
        closed,
        empty,
        full,
    };
};

// If the front and back executors are different, for now the
// mutexes and cond var types need to be the same for both.
// So we only take a single executor type and leave it to the user
// whether it's the type on the front or on the back.
// But we always take the mutex from the back.
template <class ExecT, typename T, size_t BufSize> class TQueueFront;
template <class ExecT, typename T, size_t BufSize> class TQueueBack;

template <class ExecT, typename T, size_t BufSize>
class TQueueTrunk : public TQueueBase
{
    typedef typename ExecT::Handle HandleT;
    typedef typename ExecT::CondVar CondVar;
    typedef typename ExecT::Mutex Mutex;
    typedef std::unique_lock<Mutex> Lock;
    typedef decltype(std::declval<ExecT>().context()) ExContextT;

public:
    using TQueueBase::Status;

    TQueueTrunk(ExContextT ctx)
      : notEmpty{ctx.getEmptyCondVar()}
      , notFull{ctx.getEmptyCondVar()}
      , mtx{ctx.getMutex()}
    {
    }

    // another option if we actually have handles
    TQueueTrunk(HandleT &backHandle, HandleT &frontHandle)
      : notEmpty{backHandle.getEmptyCondVar()}
      , notFull{frontHandle.getEmptyCondVar()}
      , mtx{backHandle.getMutex()}
    {
    }

private:
    void close()
    {
        Lock l{mtx};

        if (--numPushers == 0)
        {
            qClosed = true;

            l.unlock();
            notEmpty.notify_all();
        }
    }

    Status push(T data)
    {
        Lock l{mtx};

        if (qClosed) return closed;

        while (buffer.full()) notFull.wait(l);
        buffer.push(data);

        l.unlock();
        notEmpty.notify_one();
        return success;
    }
    Status wait_push(T data) { return push(data); }

    Status pop(T &data)
    {
        Status ret = success;

        Lock l{mtx};

        while (!qClosed && buffer.empty()) notEmpty.wait(l);

        if (buffer.empty())
        {
            ret = closed;
        }
        else
        {
            data = buffer.pop();
            ret = success;
        }
        l.unlock();
        notFull.notify_one();
        return ret;
    }
    Status wait_pop(T &data) { return pop(data); }

    friend class TQueueFront<ExecT, T, BufSize>;
    friend class TQueueBack<ExecT, T, BufSize>;

    Ring<T, BufSize> buffer;
    CondVar notEmpty;
    CondVar notFull;
    mutable Mutex mtx;
    int numPushers = 0;
    bool qClosed = false;
};

template <class ExecT, typename T, size_t BufSize>
class TQueueFront : public TQueueBase
{
public:
    using TQueueBase::Status;

    TQueueFront(TQueueTrunk<ExecT, T, BufSize> *base,
                typename TQueueTrunk<ExecT, T, BufSize>::HandleT &task)
      : trunk{base}
      , condVarMe{trunk->notFull.attach(task)}
    {
    }

    TQueueFront(TQueueFront &&) = delete; //no move or copy

    ~TQueueFront()
    {
        trunk->notFull.detach(condVarMe);
    }

    Status pop(T &data)
    {
        return trunk->pop(data);
    }
    Status wait_pop(T &data) { return pop(data); }

private:
    TQueueTrunk<ExecT, T, BufSize> *trunk;
    typename TQueueTrunk<ExecT, T, BufSize>::CondVar::HandleT condVarMe;
};

template <class ExecT, typename T, size_t BufSize>
class TQueueBack : public TQueueBase
{
public:
    using TQueueBase::Status;

    TQueueBack(TQueueTrunk<ExecT, T, BufSize> *base,
               typename TQueueTrunk<ExecT, T, BufSize>::HandleT &task)
      : trunk{base}
      , condVarMe{trunk->notEmpty.attach(task)}
    {
        ++(trunk->numPushers);
    }

    TQueueBack(TQueueBack &&) = delete; //no move or copy

    ~TQueueBack()
    {
        if (!closed)
        {
            trunk->notEmpty.detach(condVarMe);
        }
    }

    void close()
    {
        trunk->close();
        // we detach here, as we (should) never notify again
        trunk->notEmpty.detach(condVarMe);
        closed = true;
    }

    Status push(T data)
    {
        return trunk->push(data);
    }
    Status wait_push(T data) { return push(data); }

private:
    TQueueTrunk<ExecT, T, BufSize> *trunk;
    typename TQueueTrunk<ExecT, T, BufSize>::CondVar::HandleT condVarMe;
    bool closed = false;
};

template <class ExecT, typename T, size_t BufSize>
class TQueue : public TQueueBase
{
    typedef typename ExecT::Handle HandleT;

public:
    using TQueueBase::Status;

    TQueue(HandleT &backHandle, HandleT &frontHandle)
      : trunk{backHandle, frontHandle}
      , back{&trunk, backHandle}
      , front{&trunk, frontHandle}
    {
    }

    void close()
    {
        back.close();
    }

    Status push(T data)
    {
        return back.push(data);
    }

    Status pop(T &data)
    {
        return front.pop(data);
    }

private:
    TQueueTrunk<ExecT, T, BufSize> trunk;
    TQueueBack<ExecT, T, BufSize> back;
    TQueueFront<ExecT, T, BufSize> front;
};
#endif /* TQUEUE_HH_INCLUDED */
