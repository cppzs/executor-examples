// simple static ring buffer for queues

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#pragma once
#ifndef RING_HH_INCLUDED
#define RING_HH_INCLUDED

#include <utility>
#include <array>

template <typename T, size_t RingSize>
class Ring
{
public:
    Ring() = default;
    Ring(const Ring&) = delete;
    ~Ring() noexcept = default;

    Ring& operator =(const Ring&) = delete;

    T pop()
    {
        size_t oldHead = head;
        head = (head+1) % RingSize;
        --cnt;

        return std::move(buf[oldHead]);
    }

    template <typename TRef>
    void push(TRef &&v)
    {
        buf[tail] = std::forward<TRef>(v);
        ++cnt;
        tail = (tail+1) % RingSize;
    }

    bool empty() const
    {
        return cnt == 0;
    }

    bool full() const
    {
        return cnt == RingSize;
    }

private:
    std::array<T, RingSize> buf;

    size_t cnt = 0;
    size_t head = 0;
    size_t tail = 0;
};
#endif /* RING_HH_INCLUDED */
