// testing TQueue

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include "tqueue.hh"
#include "tgexec.hh"
#include "lest.hpp"
#include <cstddef>
#include <cassert>
#include <array>

namespace
{

const lest::test tQueueTests[] =
{
    CASE("Transfer one obj")
    {
        TPool pool;
        TGExecutor ex = pool.get_executor();

        typedef TQueue<TGExecutor, int, 1> QueueT;
        QueueT *quP;

        int curPush = 1;
        bool pubDone = false;
        auto publish = [&] ()
                       {
                           quP->push(curPush);
                           quP->close();
                           pubDone = true;
                       };

        int curPop = 0;
        bool consDone = false;
        auto consume = [&] ()
                       {
                           while (quP->pop(curPop) != QueueT::closed)
                               ;
                           consDone = true;
                       };

        auto pub = ex.twoway_execute(std::move(publish));
        auto cons = ex.twoway_execute(std::move(consume));
        QueueT qu{pub, cons};
        quP = &qu;

        EXPECT(curPop == 0);
        EXPECT(pubDone == false);
        EXPECT(consDone == false);

        auto waiter = cons.getWaiter();
        waiter.wait();
        EXPECT(curPop == 1);
        EXPECT(pubDone == false); // publisher is never resumed after close()
        EXPECT(consDone == true);
    },
    CASE("Two tails, both transferring one obj each, then closing")
    {
        TPool pool;
        TGExecutor ex = pool.get_executor();

        typedef TQueueTrunk<TGExecutor, int, 1> QTrunkT;
        typedef TQueueFront<TGExecutor, int, 1> QHeadT;
        typedef TQueueBack<TGExecutor, int, 1> QTailT;
        QTailT *qPushEnd[2];

        int curPush = 1;
        int pubProgress[2] = { -1, -1 };
        auto publish = [&] (int id)
                       {
                           pubProgress[id] = 0;
                           qPushEnd[id]->push(curPush + 10*id);
                           pubProgress[id] = 2;
                           qPushEnd[id]->close();
                           pubProgress[id] = 3;
                       };

        QHeadT *qPopEnd;

        std::array<int, 5> pops = { 0 };
        int popCount = 0;
        int consProgress = -1;
        auto consume = [&] ()
                       {
                           consProgress = 0;
                           int curPop;
                           while (qPopEnd->pop(curPop) != QHeadT::closed)
                           {
                               assert(consProgress < 5);
                               pops[consProgress++] = curPop;
                           }
                           popCount = consProgress;
                           consProgress = 99;
                       };

        auto pub1 = ex.twoway_execute([&] { publish(0); });
        auto pub2 = ex.twoway_execute([&] { publish(1); });
        auto cons = ex.twoway_execute(std::move(consume));
        QTrunkT qTrunk{ex.context()};
        QHeadT qHead{&qTrunk, cons};
        qPopEnd = &qHead;
        QTailT qTail1{&qTrunk, pub1};
        qPushEnd[0] = &qTail1;
        QTailT qTail2{&qTrunk, pub2};
        qPushEnd[1] = &qTail2;

        EXPECT(popCount == 0);
        EXPECT(pubProgress[0] == -1);
        EXPECT(pubProgress[1] == -1);
        EXPECT(consProgress == -1);

        auto waiter = cons.getWaiter();
        waiter.wait();
        EXPECT(popCount == 2);
        EXPECT(pubProgress[0] == 2); // publishers never resumed after close()
        EXPECT(pubProgress[1] == 2);
        EXPECT(consProgress == 99);
        // we don't know which one goes first
        std::array<int, 5> expPops1 = { 1, 11, 0 };
        std::array<int, 5> expPops2 = { 11, 1, 0 };
        EXPECT((pops == expPops1 || pops == expPops2));
    },
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(tQueueTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
