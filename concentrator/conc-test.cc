// test for concentrator

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "concentrator.hh"
#include "threadPool.hh"

#include <experimental/execution>

#include <iostream>
#include <lest.hpp>
#include <vector>
#include <array>

namespace execution = std::experimental::execution;

template<class Ex, class F>
Ex require(Ex ex, F&)
{
    return ex;
}


namespace
{
const lest::test concentratorTests[] =
{
    CASE("basic overall test")
    {
        auto prod1 = [] ()
        {
            static std::array<int, 5> data { 1, 3, 5, 7, 9 };
            static size_t idx = 0;
            if (idx != 5)
            {
                return data[idx++];
            }
            return 0;
        };

        auto prod2 = [] ()
        {
            static std::array<int, 5> data { 4, 2, 4, 2 };
            static size_t idx = 0;
            if (idx != 4)
            {
                return data[idx++];
            }
            return 0;
        };

        std::vector<int> result;
        auto consume = [&] (int v)
        {
            if (v % 2 == 0)
            {
                result.push_back(v);
            }
        };

        MyThreadPool pool{5};
        Concentrator<MyThreadPool
                     , int
                     , decltype(prod1)
                     , decltype(prod2)
                     , decltype(consume)
                     , 3>
            dataConcentrator(pool, prod1, prod2, consume);

        auto exec = pool.get_executor();
        dataConcentrator.run(exec);

        std::vector<int> expected = { 4, 2, 4, 2 };
        EXPECT(result == expected);
    },
};
}

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(concentratorTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
