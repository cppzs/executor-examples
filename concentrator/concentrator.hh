// concentrator.hh
/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef CONCENTRATOR_H_SEEN
#define CONCENTRATOR_H_SEEN
#include "tqueue.hh"
#include <experimental/execution>

#include <utility>

namespace execution = std::experimental::execution;

// We could give an executor type here, but it's arguably more
// correct to give an execution_context here
template <class CtxT, class T, class IN1, class IN2, class OUT, size_t bufSize>
class Concentrator
{
    // we're not consistent: for TQueue we currently require an executor type
    typedef decltype(std::declval<CtxT>().get_executor()) ExT;
    typedef TQueueBack<ExT, T, bufSize> QTailT;
    typedef TQueueFront<ExT, T, bufSize> QHeadT;
    typedef typename ExT::Handle HandleT;

public:
    Concentrator(CtxT &exCtx, IN1 producer1, IN2 producer2, OUT consumer)
      : p1(producer1)
      , p2(producer2)
      , c(consumer)
      , q(exCtx)
    {
    }

    template <class Ex>
    void run(Ex exec)
    {
        // A real twoway should call the functional with a handle
        // to itself, but we don't have this currently.
        // So we create the consuming end upfront like the producing ones below.
        // For this to work we need to wait until the queue is ready.
        // In our limited world we can use the executors cond var for it.
        typename CtxT::Mutex mtx = exec.context().getMutex();
        typedef std::unique_lock<typename CtxT::Mutex> Lock;
        typename CtxT::CondVar goCond = exec.context().getEmptyCondVar();
        bool go = false;

        using execution::require;
        QHeadT *qPullP = nullptr;
        auto exOutA = require(exec, c);
        auto exOutB = require(exOutA, execution::twoway);
        HandleT fCons
            = exOutB.twoway_execute([&, this]() -> void
                                    {
                                        {
                                            Lock l{mtx};
                                            while (!go) goCond.wait(l);
                                        }
                                        consumeWrap(qPullP);
                                    });
        QHeadT qPull{&q, fCons};
        qPullP = &qPull;

        // We need to setup the pushing ends of the queue before
        // waiting on the pulling end.
        // One option would be a yield() after setup in produceWrap().
        // We don't have yield, so we setup the queue tails here.
        QTailT *qPush1P = nullptr;
        auto exIn1A = require(exec, p1);
        auto exIn1B = require(exIn1A, execution::twoway);
        HandleT fProd1
            = exIn1B.twoway_execute([&, this]
                                    {
                                        {
                                            Lock l{mtx};
                                            while (!go) goCond.wait(l);
                                        }
                                        produceWrap<IN1>(p1, qPush1P);
                                    });
        QTailT qPush1{&q, fProd1};
        qPush1P = &qPush1;

        QTailT *qPush2P = nullptr;
        auto exIn2A = require(exec, p2);
        auto exIn2B = require(exIn2A, execution::twoway);
        HandleT fProd2
            = exIn2B.twoway_execute([&, this]
                                    {
                                        {
                                            Lock l{mtx};
                                            while (!go) goCond.wait(l);
                                        }
                                        produceWrap<IN2>(p2, qPush2P);
                                    });
        QTailT qPush2{&q, fProd2};
        qPush2P = &qPush2;

        go = true;
        goCond.notify_all();
        fCons.getWaiter().wait();
    }

private:
    template<class IN>
    void produceWrap(IN prod, QTailT *qPush)
    {
        while (true)
        {
            T data{prod()};
            if (!bool(data)) break;
            qPush->wait_push(data);
        }
        qPush->close();
    }

    void consumeWrap(QHeadT *qPop)
    {
        T data;
        while (qPop->wait_pop(data) != QHeadT::closed)
        {
            c(data);
        }
    }

    IN1 p1;
    IN2 p2;
    OUT c;
    TQueueTrunk<ExT, T, bufSize> q;
};
#endif /* CONCENTRATOR_H_SEEN */
