// tgexec.hh
//  an executor based on (stackful) coroutines

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef TGEXEC_HH_SEEN
#define TGEXEC_HH_SEEN

#include <list>
#include <utility>
#include <variant>
#include <boost/context/continuation.hpp>
#include <experimental/execution>

#include "sync-pair.hh"

class TGExecutor;
class TPool;

// We need a wrapper around continuation to make life easier.
class CtxWrapper
{
    friend class TPool;
    friend class TGExecutor;
    friend class TGEvent;
    friend class TG_test;

public:
    // this needs to be public for emplace()...
    CtxWrapper(TPool *p)
      : pool(p)
    {
    }

    // at least for tests this is public
public:
    template <class F>
    CtxWrapper(F &&fn, TPool *p)
      : ctx(boost::context::callcc([tf = std::forward<F>(fn), this]
                                   (boost::context::continuation &&c)
                                   {
                                       setPoolCtx(c.resume());
                                       tf();
                                       return getPoolCtx();
                                   }))
      , pool(p)
    {
    }

    ~CtxWrapper()
    {
    }

private:
    void setCtx(boost::context::continuation &&extCtx)
    {
        ctx = std::move(extCtx);
    }

    template <class F>
    void set(F &&fn)
    {
        ctx = boost::context::callcc([tf = std::forward<F>(fn), this]
                                     (boost::context::continuation && c) mutable
                                     {
                                         setPoolCtx(c.resume());
                                         tf();
                                         return getPoolCtx();
                                     });
    }

    void switchTo();
    void setPoolCtx(boost::context::continuation &&);
    boost::context::continuation getPoolCtx();

    boost::context::continuation ctx;
    TPool *pool;
};

class TGEvent;

class TPool
{
    friend class TGEvent;
public:
    typedef SyncCondVar<TGEvent> CondVar;
    typedef SyncMutex<TGEvent> Mutex;

    TPool() = default;
    TPool(TPool const &) = delete;
    TPool(TPool &&) = delete;
    ~TPool()
    {
        for (auto &t : bag)
        {
            (void)(t); // silence warning
            //if (t.unfinished()) t.switchTo(); // this may be an option
        }
    }

    TGExecutor get_executor();

    // this doesn't work in general as std::mutex isn't copyable/movable
    SyncMutex<TGEvent> getMutex() const;

    SyncCondVar<TGEvent> getEmptyCondVar() const;

private:
    friend class CtxWrapper;
    friend class TGExecutor;
    friend class TG_test;  // testing backdoor

    CtxWrapper *enqueue()
    {
        return &bag.emplace_back(this);
    }

    template <class F>
    void spawn(F &&fun)
    {
        CtxWrapper *task{&bag.emplace_back(std::forward<F>(fun), this)};
        if (!current)
        {
            task->switchTo();
        }
    }

    // We need stable addresses
    // and we never iterate except (possibly) for dtor.
    // So just use std::list.
    std::list<CtxWrapper> bag;
    CtxWrapper *current = nullptr;
    CtxWrapper *oldCurrent = nullptr;
    boost::context::continuation ctx;
    mutable CtxWrapper extTask{this};
};

class TGEvent
{
public:
    typedef CtxWrapper *TaskPtr;

    TGEvent(CtxWrapper *signalTask)
      : signallingTask{signalTask}
      , blockingTask{nullptr}
      , pool{signalTask->pool}
    {
    }
    // TGEvent is not really movable, but for special cases we need a move anyway
    // We should probably just make it movable.
    TGEvent(TGEvent &&) = delete;
    TGEvent(TGEvent &&other, int)
      : signallingTask{other.signallingTask}
      , blockingTask{other.blockingTask}
      , pool{other.pool}
    {
        other.signallingTask = nullptr;
        other.blockingTask = nullptr;
        other.pool = nullptr;
    }
    // Special copy is nedded for CondVar.
    // With attach() this may not be needed anymore.
    // But in any case we should never copy the blockingTask.
    TGEvent(TGEvent const &) = delete;
    TGEvent(TGEvent const &other, std::nullptr_t)
      : signallingTask{other.signallingTask}
      , blockingTask{nullptr}
      , pool{other.pool}
    {
    }

    // kind of constructor (for mutex)
    static TGEvent createEmpty(TGEvent &other)
    {
        return {other.pool};
    }

    void signal()
    {
        if (blockingTask)
        {
            CtxWrapper *blocker = blockingTask;
            blockingTask = nullptr;
            blocker->switchTo();
        }
    }

    void block()
    {
        // We treat blocking on an already blocked task as deadlock
        // => undefined behaviour
        if (signallingTask)
        {
            // we might be the initial block and don't have a current yet
            if (pool->current)
            {
                blockingTask = pool->current;
            }
            else
            {
                blockingTask = &(pool->extTask);
            }
            signallingTask->switchTo();
        }
    }

    // we need this for switching from one cond var notifier to another
    void transfer()
    {
        if (blockingTask)
        {
            signallingTask->switchTo();
        }
    }

    CtxWrapper *hasSignallingTask() const
    {
        return signallingTask;
    }

    bool isBlocked() const
    {
        return blockingTask;
    }

    void setSignalling(CtxWrapper *tsk)
    {
        signallingTask = tsk;
    }

    void setSignallingCurrent()
    {
        signallingTask = pool->current;
    }

private:
    friend class TG_test;  // testing backdoor
    friend class TPool;

    TGEvent() = default;

    TGEvent(TPool const *p)
      : pool(p)
    {
    }

    CtxWrapper *signallingTask = nullptr;
    CtxWrapper *blockingTask = nullptr;
    TPool const *pool = nullptr;
};


class TGExecutor
{
public:
    typedef SyncHandle<TGEvent> Handle;
    typedef SyncPreHandle<TGEvent> TGPreHandle;
    typedef SignalHandle<TGEvent> TGSignalHandle;
    typedef SyncCondVar<TGEvent> CondVar;
    typedef SyncMutex<TGEvent> Mutex;

    template <class F>
    void execute(F &&fun)
    {
        ctx->spawn(std::forward<F>(fun));
    }

    // twoway_execute doesn't start, but needs boost-blocking
    template <class F>
    Handle twoway_execute(F &&fun)
    {
        CtxWrapper *tsk{ctx->enqueue()};
        TGPreHandle preTask{tsk};
        tsk->set([f = std::forward<F>(fun), signaller = TGSignalHandle{&preTask}]
                 () mutable
                 {
                     f();
                     signaller.signal();
                 });
        //tsk->switchTo(); => we don't start, but wait for boost blocking
        return {std::move(preTask)};
    }

    friend bool operator==(TGExecutor const & a, TGExecutor const & b) noexcept
    {
        return a.ctx == b.ctx;
    }
    friend bool operator!=(TGExecutor const & a, TGExecutor const & b) noexcept
    {
        return !(a == b);
    }
    auto &context() const noexcept { return *ctx; }
    TGExecutor require(decltype(std::experimental::execution::twoway) const &) { return *this; }

private:
    friend class TPool;

    TGExecutor(TPool *tp)
      : ctx(tp)
    {}

    TPool *ctx;
};

inline TGExecutor TPool::get_executor()
{
    return {this};
}

inline void CtxWrapper::switchTo()
{
    if (ctx)
    {
        if (pool->current)
        {
            pool->oldCurrent = pool->current;
        }
        else
        {
            // we're called from outside the pool
            // so we leave oldCurrent at nullptr
            // so setPoolCtx will store this in extTask
        }
        pool->current = this;
        boost::context::continuation tmp = ctx.resume();
        if (pool->oldCurrent) pool->oldCurrent->ctx = std::move(tmp);
    }
}

inline void CtxWrapper::setPoolCtx(boost::context::continuation &&ctx_)
{
    if (pool->oldCurrent)
    {
        pool->oldCurrent->ctx = std::move(ctx_);
    }
    else
    {
        pool->extTask.setCtx(std::move(ctx_));
    }
}

boost::context::continuation CtxWrapper::getPoolCtx()
{
    return ctx ? std::move(ctx) : std::move(pool->extTask.ctx);
}

SyncMutex<TGEvent> TPool::getMutex() const
{
    return TGEvent(this);
}

SyncCondVar<TGEvent> TPool::getEmptyCondVar() const
{
    return TGEvent(this);
}

#endif /* TGEXEC_HH_SEEN */
