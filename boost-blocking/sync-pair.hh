// sync-pair.hh
// a try to generalize a promise/futurelike pair w/o heap allocation
// any end of the pair can go away first

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#pragma once
#ifndef SYNC_PAIR_HH_SEEN
#define SYNC_PAIR_HH_SEEN

#include <list>
#include <utility>
#include <array>
#include <cassert>

template <class Event> class SyncHandle;

// a mutex can be waited for by many tasks
// we think about this later
template <class Event>
class SyncMutex
{
public:
    SyncMutex(Event &&e) : evt{std::move(e), 0} {}

    void lock()
    {
        if (evt.hasSignallingTask())
        {
            evt.block();
        }
        evt.setSignallingCurrent();
    }
    bool try_lock()
    {
        if (evt.hasSignallingTask())
        {
            return false;
        }
        evt.setSignallingCurrent();
        return true;
    }
    void unlock()
    {
        if (evt.isBlocked())
        {
            evt.signal();
        }
        evt.setSignalling(nullptr);
    }

private:
    friend class SyncHandle<Event>;

    Event evt;
};

// A condition variable in a boost-blocking world means we have to store
// all notifier tasks.
// On which specific one to block is a policy question, but we can't
// block on a task that doesn't notify anymore.
// So we need a mechanism to detach and attach.
// To avoid dynamic memory allocation we have just a fixed sized buffer.
template <class Event>
class SyncCondVar
{
public:
    typedef int HandleT;

    SyncCondVar(Event &&e)
      : evt{std::move(e), 0}
      , size{1}
      , notifiers{evt.hasSignallingTask(), nullptr}
    {
        if (notifiers[0] == nullptr)
        {
            size = 0;
        }
    }

    HandleT attach(SyncHandle<Event> &task)
    {
        assert(size != MaxSize);
        notifiers[size] = task.getTask();
        evt.setSignalling(notifiers[size]);
        return size++;
    }

    // we keep the current size (i.e. don't really support dynamic at/de-taching)
    void detach(HandleT idx)
    {
        auto oldTask = notifiers[idx];
        notifiers[idx] = nullptr;
        if (evt.hasSignallingTask() == oldTask)
        {
            typename Event::TaskPtr newNotifier = nullptr;
            for (auto p: notifiers)
            {
                if (p)
                {
                    newNotifier = p;
                    break;
                }
            }
            evt.setSignalling(newNotifier);
            evt.transfer();
        }
    }

    void notify_one()
    {
        evt.signal();
    }

    void notify_all()
    {
        notify_one();
    }

    template <class Mutex>
    void wait(Mutex &mtx)
    {
        mtx.unlock();
        evt.block();
        mtx.lock();
    }

private:
    friend class SyncHandle<Event>;

    Event evt;
    static constexpr int MaxSize = 5;
    int size = 0;
    std::array<typename Event::TaskPtr, MaxSize> notifiers = {nullptr};
};

template <class Event> class SignalHandle;
template <class Event> class SyncWaiter;
template <class Event> class SyncPreHandle;
template <class Event> class SyncHandle;

// first a helper
template <class Event>
class EventMover
{
    friend class SignalHandle<Event>;
    friend class SyncCondVar<Event>;
    friend class SyncWaiter<Event>;
    friend class SyncPreHandle<Event>;
    friend class SyncHandle<Event>;

    EventMover(typename Event::TaskPtr task)
      : evt{task}
    {}
    EventMover(EventMover &&rhs)
      : evt{std::move(rhs.evt), 0}
      , signaller{rhs.signaller}
    {
        if (signaller)
        {
            signaller->evtHandle = this;
            rhs.signaller = nullptr;
        }
    }
    ~EventMover()
    {
        if (signaller)
        {
            signaller->evtHandle = nullptr;
        }
    }

    typename Event::TaskPtr getTask()
    {
        return evt.hasSignallingTask();
    }

    Event evt;
    SignalHandle<Event> *signaller = nullptr;
};

template <class Event>
class SyncPreHandle : public EventMover<Event>
{
public:

    SyncPreHandle(typename Event::TaskPtr task)
      : EventMover<Event>(task)
    {}
};

template <class Event>
class SyncWaiter : public EventMover<Event>
{
public:
    void wait()
    {
        EventMover<Event>::evt.block();
    }

private:
    friend class SyncHandle<Event>;
    friend class SignalHandle<Event>;

    SyncWaiter(EventMover<Event> &&rhs)
      : EventMover<Event>{std::move(rhs)}
    {
    }
};

// SignalHandle holds a pointer to an event that can be signalled.
// If the event moves, this handle gets updated
// (as we don't want objects on the heap).
template <class Event>
class SignalHandle
{
    friend class EventMover<Event>;
    friend class SyncHandle<Event>;
public:
    SignalHandle(SyncPreHandle<Event> *pre)
      : evtHandle{pre}
    {
        evtHandle->signaller = this;
    }

    SignalHandle(SignalHandle &&orig)
      : evtHandle{orig.evtHandle}
    {
        orig.evtHandle = nullptr;
        evtHandle->signaller = this;
    }

    ~SignalHandle()
    {
        signal();
        if (evtHandle)
        {
            evtHandle->evt.setSignalling(nullptr);
        }
    }

    SignalHandle &operator=(SignalHandle &&) = delete;

    // Current semantics: only one signal() possible.
    // With boost-blocking semantics this probably makes most sense
    // and it's correct for promise like things.
    // The underlying event does not have this semantics
    // (as e.g. condition variables can be notifid arbitrarily often).
    void signal()
    {
        if (evtHandle)
        {
            evtHandle->evt.signal();
            evtHandle->evt.setSignalling(nullptr);
            evtHandle->signaller = nullptr;
        }
        evtHandle = nullptr;
    }

private:
    EventMover<Event> *evtHandle = nullptr;
};

template <class Event>
class SyncHandle : public EventMover<Event>
{
public:
    SyncHandle(SyncPreHandle<Event> &&pre)
      : EventMover<Event>{std::move(pre)}
    {
        if (EventMover<Event>::signaller)
        {
            EventMover<Event>::signaller->evtHandle = this;
        }
    }
    // This is something like a future and should be movable.
    // Now we probably don't need PreHandle anymore...
    // But maybe we want to make it unmovable somehow after
    // we handed out some sync obj depneding on us...
    SyncHandle(SyncHandle &&rhs)
      : EventMover<Event>{std::move(rhs)}
    {
        if (EventMover<Event>::signaller)
        {
            EventMover<Event>::signaller->evtHandle = this;
        }
    }
    ~SyncHandle()
    {
        if (EventMover<Event>::signaller)
        {
            EventMover<Event>::signaller->evtHandle = nullptr;
        }
    }

    SyncHandle& operator=(SyncHandle &&) = delete;

    SyncWaiter<Event> getWaiter() { return std::move(*this); }

    // What if the task is already gone?
    // Ee don't really care
    SyncMutex<Event> getMutex()
    {
        return Event::createEmpty(EventMover<Event>::evt);
    }
    SyncCondVar<Event> getCondVar()
    {
        return {Event{EventMover<Event>::evt, nullptr}};
    }
    SyncCondVar<Event> getEmptyCondVar()
    {
        return Event::createEmpty(EventMover<Event>::evt);
    }
    Event &getEvent()
    {
        return EventMover<Event>::evt;
    }
};
#endif /* SYNC_PAIR_HH_SEEN */
