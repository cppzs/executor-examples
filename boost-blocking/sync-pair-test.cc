/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "sync-pair.hh"

#include "lest.hpp"


namespace
{
// we need a test event class
struct TestEvent
{
    typedef std::function<void(TestEvent *)> *TaskPtr;

    TestEvent(TaskPtr signalTask)
      : signallingTask{signalTask}
      , blockingTask{nullptr}
      , debugingTask{signalTask}
    {
        currentEvt = this;
        ++evtCount;
        ++nonMovedEvtCount;
    }
    // Event is not really movable, but for special cases we need a move anyway.
    // We should probably just make it movable.
    TestEvent(TestEvent &&) = delete;
    TestEvent(TestEvent &&other, int)
      : signallingTask{other.signallingTask}
      , blockingTask{other.blockingTask}
      , debugingTask{other.debugingTask}
    {
        other.signallingTask = nullptr;
        other.blockingTask = nullptr;
        other.movedFrom = true;
        currentEvt = this;
        ++evtCount;
    }
    // Special copy is nedded for CondVar.
    // With attach() this may not be needed anymore.
    // But in any case we should never copy the blockingTask
    TestEvent(TestEvent const &) = delete;
    TestEvent(TestEvent const &other, std::nullptr_t)
      : signallingTask{other.signallingTask}
      , blockingTask{nullptr}
      , debugingTask{other.debugingTask}
    {
        currentEvt = this;
        ++evtCount;
        ++nonMovedEvtCount;
    }

    ~TestEvent()
    {
        --evtCount;
        if (!movedFrom)
        {
            --nonMovedEvtCount;
        }
    }

    // kind of default constructor
    static TestEvent createEmpty(TestEvent &)
    {
        return {};
    }

    void signal()
    {
        (*debugingTask)(this);
        //signallingTask = nullptr;
        if (blockingTask)
        {
            (*blockingTask)(this);
            ++signalCount;
        }
        blockingTask = nullptr;
    }

    void block()
    {
        if (signallingTask)
        {
            blockingTask = testCurrent;
            (*debugingTask)(this);
            (*signallingTask)(this);
            ++blockCount;
        }
    }

    TaskPtr hasSignallingTask() const
    {
        return signallingTask;
    }

    bool isBlocked() const
    {
        return blockingTask;
    }

    void setSignalling(TaskPtr tsk)
    {
        signallingTask = tsk;
    }

    void setSignallingCurrent()
    {
        signallingTask = testCurrent;
    }

    // test checker
    TaskPtr getBlockingTask() const { return blockingTask; }
    TaskPtr getSignallingTask() const { return signallingTask; }

    TaskPtr signallingTask = nullptr;
    TaskPtr blockingTask = nullptr;
    TaskPtr debugingTask = nullptr;
    TaskPtr testCurrent = nullptr;
    bool movedFrom = false;

    static TestEvent *currentEvt;
    static int signalCount;
    static int blockCount;
    static int evtCount;
    static int nonMovedEvtCount;

private:
    TestEvent()
    {
        currentEvt = this;
        ++nonMovedEvtCount;
        ++evtCount;
    }
};

TestEvent *TestEvent::currentEvt = nullptr;
int TestEvent::signalCount = 0;
int TestEvent::blockCount = 0;
int TestEvent::evtCount = 0;
int TestEvent::nonMovedEvtCount = 0;

const lest::test spTests[] =
{
    CASE("Create a pair SignalHandle/SyncHandle and check correct identity")
    {
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;

        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f};
        EXPECT(TestEvent::currentEvt != nullptr);
        EXPECT(TestEvent::nonMovedEvtCount == 1);

        TestEvent *e1 = TestEvent::currentEvt;
        TestEvent::currentEvt = nullptr;

        SignalHandle<TestEvent> snd{&pre};

        SyncHandle<TestEvent> rcv{std::move(pre)};
        EXPECT(TestEvent::currentEvt != nullptr);
        EXPECT(TestEvent::nonMovedEvtCount == 1);
        
        e1 = TestEvent::currentEvt;

        snd.signal();
        EXPECT(TestEvent::signalCount == signalCnt);
        EXPECT(e1 == eCalled);
    },
    CASE("Create a waiter and check identity")
    {
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;
        int blockCnt = TestEvent::blockCount;

        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f};
        SignalHandle<TestEvent> snd{&pre};
        SyncHandle<TestEvent> rcv{std::move(pre)};
        TestEvent *e1 = TestEvent::currentEvt;

        SyncWaiter<TestEvent> waiter = rcv.getWaiter();
        EXPECT(e1 != TestEvent::currentEvt);
        e1 = TestEvent::currentEvt;

        e1->testCurrent = &f;
        waiter.wait();
        EXPECT(TestEvent::blockCount == blockCnt + 1);
        EXPECT(e1 == eCalled);
        EXPECT(e1->blockingTask == &f);
        EXPECT(e1->signallingTask == &f);

        snd.signal();
        EXPECT(TestEvent::signalCount == signalCnt + 1);
        EXPECT(e1 == eCalled);
    },
    CASE("Create a waiter after SignalHandler is gone")
    {
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;
        int blockCnt = TestEvent::blockCount;

        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };

        std::byte sndStorage[sizeof(SignalHandle<TestEvent>)];

        SyncPreHandle<TestEvent> pre{&f};
        auto snd = new (sndStorage) SignalHandle<TestEvent>{&pre};
        SyncHandle<TestEvent> rcv{std::move(pre)};
        TestEvent *e1 = TestEvent::currentEvt;

        snd->signal();
        EXPECT(TestEvent::signalCount == signalCnt);
        EXPECT(e1 == eCalled);
        eCalled = nullptr;

        snd->~SignalHandle<TestEvent>();
        EXPECT(e1->getSignallingTask() == nullptr);

        SyncWaiter<TestEvent> waiter = rcv.getWaiter();
        EXPECT(e1 != TestEvent::currentEvt);
        e1 = TestEvent::currentEvt;

        TestEvent *dummy = nullptr;
        ++dummy;
        eCalled = dummy;
        waiter.wait();
        EXPECT(TestEvent::blockCount == blockCnt);
        EXPECT(eCalled == dummy);
    },
    CASE("Signal after Waiter is gone")
    {
        EXPECT(TestEvent::nonMovedEvtCount == 0);
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;

        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };
        std::byte waiterStorage[sizeof(SyncWaiter<TestEvent>)];

        SyncPreHandle<TestEvent> pre{&f};
        SignalHandle<TestEvent> snd{&pre};
        SyncHandle<TestEvent> rcv{std::move(pre)};
        auto waiter = new (waiterStorage) SyncWaiter<TestEvent>{rcv.getWaiter()};

        waiter->~SyncWaiter<TestEvent>();
        EXPECT(TestEvent::nonMovedEvtCount == 0);

        TestEvent *dummy = nullptr;
        ++dummy;
        eCalled = dummy;
        snd.signal();
        EXPECT(TestEvent::signalCount == signalCnt);
        EXPECT(eCalled == dummy);
    },
    CASE("Mutex creation")
    {
        EXPECT(TestEvent::nonMovedEvtCount == 0);
        TestEvent *eCalled = nullptr;
        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f};
        EXPECT(TestEvent::nonMovedEvtCount == 1);
        SyncHandle<TestEvent> handle{std::move(pre)};
        TestEvent *e1 = TestEvent::currentEvt;
        EXPECT(TestEvent::nonMovedEvtCount == 1);

        SyncMutex<TestEvent> mtx = handle.getMutex();
        EXPECT(TestEvent::currentEvt != nullptr);
        EXPECT(TestEvent::currentEvt != e1);
        e1 = TestEvent::currentEvt;
        EXPECT(TestEvent::nonMovedEvtCount == 2);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == nullptr);
    },
    CASE("Mutex simple lock")
    {
        TestEvent *eCalled = nullptr;
        int blockCnt = TestEvent::blockCount;

        std::function<void(TestEvent *)> f1
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f1};
        SyncHandle<TestEvent> handle{std::move(pre)};

        SyncMutex<TestEvent> mtx = handle.getMutex();
        TestEvent *e1 = TestEvent::currentEvt;
        e1->debugingTask = &f1;

        e1->testCurrent = &f1;
        mtx.lock();
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f1);
        EXPECT(TestEvent::blockCount == blockCnt);
    },
    CASE("Mutex lock")
    {
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;
        int blockCnt = TestEvent::blockCount;

        std::function<void(TestEvent *)> f1
        = [&] (TestEvent *evt) { eCalled = evt; };
        std::function<void(TestEvent *)> f2
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f1};
        SyncHandle<TestEvent> handle{std::move(pre)};

        SyncMutex<TestEvent> mtx = handle.getMutex();
        TestEvent *e1 = TestEvent::currentEvt;
        e1->debugingTask = &f1;

        e1->testCurrent = &f1;
        mtx.lock();

        e1->testCurrent = &f2;
        mtx.unlock();
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == nullptr);
        EXPECT(TestEvent::signalCount == signalCnt);

        // for boost blocking, a second lock runs the signaller
        // to completion (i.e. until unlock())
        // so we need to give something that unlocks
        std::function<void(TestEvent *)> f3
        = [&] (TestEvent *evt) { eCalled = evt; mtx.unlock(); };
        e1->testCurrent = &f3;
        mtx.lock();
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f3);
        EXPECT(TestEvent::blockCount == blockCnt);

        e1->testCurrent = &f1;
        mtx.lock();
        // now we got an unlock() (from f3)
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f1);
        EXPECT(TestEvent::blockCount == blockCnt + 1);

        e1->testCurrent = &f2;
        mtx.unlock();
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == nullptr);
        EXPECT(TestEvent::signalCount == signalCnt + 1);
    },
    CASE("Mutex try_lock")
    {
        TestEvent *eCalled = nullptr;
        int signalCnt = TestEvent::signalCount;
        int blockCnt = TestEvent::blockCount;

        std::function<void(TestEvent *)> f1
        = [&] (TestEvent *evt) { eCalled = evt; };
        std::function<void(TestEvent *)> f2
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f1};
        SyncHandle<TestEvent> handle{std::move(pre)};

        SyncMutex<TestEvent> mtx = handle.getMutex();
        TestEvent *e1 = TestEvent::currentEvt;
        e1->debugingTask = &f1;

        e1->testCurrent = &f1;
        bool gotLock = mtx.try_lock();
        EXPECT(gotLock == true);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f1);
        EXPECT(TestEvent::blockCount == blockCnt);

        e1->testCurrent = &f2;
        mtx.unlock();
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == nullptr);
        EXPECT(TestEvent::signalCount == signalCnt);

        e1->testCurrent = &f1;
        gotLock = mtx.try_lock();
        EXPECT(gotLock == true);
        e1->testCurrent = &f2;
        gotLock = mtx.try_lock();
        EXPECT(gotLock == false);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f1);
        EXPECT(TestEvent::blockCount == blockCnt);
    },
    CASE("Condition variable creation")
    {
        EXPECT(TestEvent::nonMovedEvtCount == 0);
        TestEvent *eCalled = nullptr;
        std::function<void(TestEvent *)> f
        = [&] (TestEvent *evt) { eCalled = evt; };

        SyncPreHandle<TestEvent> pre{&f};
        SyncHandle<TestEvent> handle{std::move(pre)};
        SyncMutex<TestEvent> mtx = handle.getMutex();
        TestEvent *e1 = TestEvent::currentEvt;
        e1->debugingTask = &f;

        SyncCondVar<TestEvent> cond = handle.getCondVar();
        EXPECT(TestEvent::currentEvt != nullptr);
        EXPECT(TestEvent::currentEvt != e1);
        e1 = TestEvent::currentEvt;
        EXPECT(TestEvent::nonMovedEvtCount == 3);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f);
    },
    CASE("Condition variable wait()")
    {
        EXPECT(TestEvent::nonMovedEvtCount == 0);
        TestEvent *eCalled1 = nullptr;
        SyncCondVar<TestEvent> *condPtr = nullptr;
        std::function<void(TestEvent *)> f1
        = [&] (TestEvent *evt) { eCalled1 = evt; condPtr->notify_one(); };

        TestEvent *eCalled2 = nullptr;
        int dbgCount1 = 0;
        std::function<void(TestEvent *)> f2
        = [&] (TestEvent *evt) { ++dbgCount1; eCalled2 = evt; };

        int dbgCount2 = 0;
        std::function<void(TestEvent *)> f3
        = [&] (TestEvent *evt) { ++dbgCount2; };

        SyncPreHandle<TestEvent> pre{&f1};
        SyncHandle<TestEvent> handle{std::move(pre)};
        SyncMutex<TestEvent> mtx = handle.getMutex();
        TestEvent *e1 = TestEvent::currentEvt;
        e1->debugingTask = &f2;
        SyncCondVar<TestEvent> cond = handle.getCondVar();
        condPtr = &cond;
        TestEvent *e2 = TestEvent::currentEvt;

        e1->testCurrent = &f3;
        mtx.lock();
        EXPECT(dbgCount1 == 0);

        e2->debugingTask = &f2;
        cond.wait(mtx);
        // we have:
        //  mtx.unlock() on non-blocked  => no signal()
        //  block() on cond: ++dbgCount1
        //  f1() => notify() => signal(): ++dbgCount1
        //  mtx.lock() on non-blocked => no block()
        EXPECT(dbgCount1 == 2);
        EXPECT(dbgCount2 == 0);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f3);

        EXPECT(mtx.try_lock() == false);
    },
    CASE("Condition variable non-waited notify()")
    {
        EXPECT(TestEvent::nonMovedEvtCount == 0);
        int signalCnt = TestEvent::signalCount;
        TestEvent *eCalled1 = nullptr;
        std::function<void(TestEvent *)> f1
        = [&] (TestEvent *evt) { eCalled1 = evt; };

        TestEvent *eCalled2 = nullptr;
        int dbgCount = 0;
        std::function<void(TestEvent *)> f2
        = [&] (TestEvent *evt) { ++dbgCount; eCalled2 = evt; };

        SyncPreHandle<TestEvent> pre{&f1};
        SyncHandle<TestEvent> handle{std::move(pre)};
        SyncCondVar<TestEvent> cond = handle.getCondVar();
        TestEvent *e1 = TestEvent::currentEvt;

        e1->debugingTask = &f2;
        cond.notify_one();
        EXPECT(dbgCount == 1);
        EXPECT(TestEvent::signalCount == signalCnt);
        EXPECT(e1->getBlockingTask() == nullptr);
        EXPECT(e1->getSignallingTask() == &f1);
        EXPECT(eCalled1 == nullptr);
    },
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(spTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
