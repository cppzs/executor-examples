// tgevent-test.cc

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include "tgexec.hh"
#include "lest.hpp"
#include <cstddef>


struct TG_test // testing backdoor
{
    static CtxWrapper *enqueue(TPool &p)
    {
        return p.enqueue();
    }
    template <class F> static void spawn(TPool &p, F &&foo)
    {
        p.spawn(std::forward<F>(foo));
    }
    template <class F> static void set(CtxWrapper *ctx, F &&fn)
    {
        ctx->set(std::forward<F>(fn));
    }
};

namespace
{

const lest::test tgEvtTests[] =
{
    CASE("Create an event")
    {
        TPool pool;
        int tskCallCnt = 0;
        CtxWrapper tsk{[&] { ++tskCallCnt; }, &pool};

        TGEvent evt{&tsk};
        EXPECT(bool(evt.hasSignallingTask()) == true);
        EXPECT(evt.isBlocked() == false);
        EXPECT(tskCallCnt == 0);
    },
    CASE("Block on an event with a task")
    {
        TPool pool;
        int tskCallCnt = 0;
        TGEvent *evtPtr = nullptr;
        bool goOn = true;
        bool sigDone = false;
        auto sig([&]
                 {
                     do
                     {
                         ++tskCallCnt;
                         evtPtr->signal();
                     } while (goOn);
                     sigDone = true;
                 });
        CtxWrapper *sigCtx{TG_test::enqueue(pool)};
        TG_test::set(sigCtx, sig);
        TGEvent evt{sigCtx};
        evtPtr = &evt;

        auto tester{[&]
                    {
                        evt.block();
                        EXPECT(bool(evt.hasSignallingTask()) == true);
                        EXPECT(evt.isBlocked() == false);
                        EXPECT(tskCallCnt == 1);
                    }};

        TG_test::spawn(pool, tester);
        EXPECT(bool(evt.hasSignallingTask()) == true);
        EXPECT(evt.isBlocked() == false);
        EXPECT(tskCallCnt == 1);
        EXPECT(sigDone == false); // we never finish sig
    },
};
} // unnamed namespace

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(tgEvtTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
