// test program for TGExecutor

/*
 * Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "tgexec.hh"

#include "lest.hpp"

#include <vector>

// TG_test needs to be in the same namespace than TGEvent and CtxWrapper
class TG_test // testing backdoor
{
public:
    static CtxWrapper *getTask(TGEvent &evt)
    {
        return evt.signallingTask;
    }

    static void switchTo(CtxWrapper *tsk)
    {
        tsk->switchTo();
    }
};

namespace
{
const lest::test tgExecTests[] =
{
    CASE("Simple execute()")
    {
        TPool pool;
        TGExecutor ex{pool.get_executor()};

        int val = 0;

        ex.execute([&] { val = 1; });
        EXPECT(val == 1);
    },
    CASE("Wait for a two-way")
    {
        TPool pool;
        TGExecutor ex{pool.get_executor()};

        int val = 0;

        auto f1 = ex.twoway_execute([&] { val = 1; });
        f1.getWaiter().wait();
        EXPECT(val == 1);
    },
    CASE("Cleanup, waiting for both tasks")
    {
        int obj1Count = 0;
        int obj2Count = 0;

        struct ObjLog
        {
            ObjLog(int &cnt)
              : counter{&cnt}
            {
                ++(*counter);
            }
            ~ObjLog()
            {
                --(*counter);
            }
            int *counter;
        };

        TPool pool;
        TGExecutor ex{pool.get_executor()};
        TGEvent *evt;

        auto tf1 = [&]
                   {
                       ObjLog o{obj1Count};
                       evt->block();
                   };

        int i = 0;
        auto tf2 = [&]
                   {
                       ObjLog o{obj2Count};
                       ++i;
                       evt->signal();
                       ++i;
                   };

        auto f1 = ex.twoway_execute(tf1);
        auto f2 = ex.twoway_execute(tf2);
        evt = &f2.getEvent();

        EXPECT(obj1Count == 0);
        EXPECT(obj2Count == 0);
        EXPECT(i == 0);

        f1.getWaiter().wait();

        EXPECT(i == 1);
        EXPECT(obj1Count == 0);
        EXPECT(obj2Count == 1); // tf2 isn't finished yet

        f2.getWaiter().wait();
        EXPECT(i == 2);
        EXPECT(obj2Count == 0);
    },
    CASE("Cleanup, waiting for first task only")
    {
        int obj1Count = 0;
        int obj2Count = 0;
        int i = 0;

        {
            struct ObjLog
            {
                ObjLog(int &cnt)
                  : counter{&cnt}
                {
                    ++(*counter);
                }
                ~ObjLog()
                {
                    --(*counter);
                }
                int *counter;
            };

            TPool pool;
            TGExecutor ex{pool.get_executor()};
            TGEvent *evt;

            auto tf1 = [&]
                       {
                           ObjLog o{obj1Count};
                           EXPECT(obj1Count == 1);
                           EXPECT(obj2Count == 0);
                           evt->block();
                           EXPECT(obj1Count == 1);
                           EXPECT(obj2Count == 1);
                       };

            auto tf2 = [&]
                       {
                           ObjLog o{obj2Count};
                           EXPECT(obj1Count == 1);
                           EXPECT(obj2Count == 1);
                           ++i;
                           evt->signal();
                           ++i;
                           EXPECT(false); // never reached
                       };

            auto f1 = ex.twoway_execute(tf1);
            auto f2 = ex.twoway_execute(tf2);
            evt = &f2.getEvent();

            EXPECT(obj1Count == 0);
            EXPECT(obj2Count == 0);
            EXPECT(i == 0);

            f1.getWaiter().wait();

            EXPECT(i == 1);
            EXPECT(obj1Count == 0);
            EXPECT(obj2Count == 1); // tf2 isn't finished
        }
        EXPECT(i == 1);
        EXPECT(obj1Count == 0);
        EXPECT(obj2Count == 0);
    },
    CASE("Run a ping-pong three times")
    {
        TPool pool;
        TGExecutor ex{pool.get_executor()};
        TGEvent *evt;

        std::vector<int> seq;

        auto tf1 = [&]
                   {
                       seq.push_back(1);
                       evt->block();
                       seq.push_back(1);
                       evt->block();
                       seq.push_back(1);
                       evt->block();
                       seq.push_back(1);
                   };

        auto tf2 = [&]
                   {
                       seq.push_back(2);
                       evt->signal();
                       seq.push_back(2);
                       evt->signal();
                       seq.push_back(2);
                       evt->signal();
                       seq.push_back(2);
                   };

        auto f1 = ex.twoway_execute(tf1);
        auto f2 = ex.twoway_execute(tf2);
        evt = &f2.getEvent();
        f1.getWaiter().wait();
        std::vector<int> expSeq { 1, 2, 1, 2, 1, 2, 1 };
        EXPECT(seq[0] == 1);
        EXPECT(seq == expSeq);
    },
    CASE("Switch randomly")
    {
        TPool pool;
        TGExecutor ex{pool.get_executor()};

        constexpr int numTasks = 5;
        constexpr int numSwitches = 10;

        std::vector<TGExecutor::Handle> handles;
        std::vector<CtxWrapper *> tasks;
        std::vector<int> testSeq;
        std::vector<int> resultSeq;
        int curIdx = 0;

        for (int i = 0; i != numSwitches; ++i)
        {
            static int lastId = numTasks;
            int nextId;
            do
            {
                nextId = rand() % numTasks;
            } while (nextId == lastId); // no same switches for now
            lastId = nextId;
            testSeq.push_back(nextId);
        }

        auto testTask = [&] (int id)
                        {
                            while (curIdx < numSwitches)
                            {
                                resultSeq.push_back(id);
                                TG_test::switchTo(tasks[testSeq[curIdx++]]);
                            }
                            resultSeq.push_back(id);
                        };

        for (int i = 0; i != numTasks; ++i)
        {
            handles.emplace_back(ex.twoway_execute([&, i] { testTask(i); }));
            tasks.push_back(TG_test::getTask(handles[i].getEvent()));
        }

        TG_test::switchTo(tasks[testSeq[curIdx++]]);

        EXPECT(curIdx == numSwitches);
        EXPECT(testSeq == resultSeq);
    },
};
} // unnamed namepsace

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(tgExecTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
