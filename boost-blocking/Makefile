# Makefile for context switch based executor

# Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
#
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

TOP = ..

VPATH = $(CONTEXT_DIR)/src:$(CONTEXT_DIR)/src/posix:$(CONTEXT_DIR)/src/asm

INCLUDES += -I$(TOP)/concentrator -I. -I$(TOP)/queue

HEADERS = \
	../queue/tqueue.hh \
	../queue/ring.hh \
	sync-pair.hh \
	tgexec.hh \
	../concentrator/concentrator.hh \


TARGETS = \
	tgexec-test \
	sync-pair-test \
	tgevent-test \
	tgcollector \


OBJS = \
	sync-pair-test.o \
	tgexec-test.o \
	tgevent-test.o \
	tgcollector.o \


CONTEXT_OBJS = \
	execution_context.o \
	stack_traits.o \
	make_x86_64_sysv_elf_gas.o \
	jump_x86_64_sysv_elf_gas.o \
	ontop_x86_64_sysv_elf_gas.o \


all: $(TARGETS)

TEST_TARGETS = \
	do_sync-pair-tests \
	do_tgevent-tests \
	do_tgexec-tests \
	do_tgcollector \


tests: $(TEST_TARGETS)

$(OBJS): $(HEADERS)

tgexec-test: tgexec-test.o $(CONTEXT_OBJS)
sync-pair-test: sync-pair-test.o
tgevent-test: tgevent-test.o $(CONTEXT_OBJS)
tgcollector: tgcollector.o $(CONTEXT_OBJS)

do_sync-pair-tests: sync-pair-test
	./sync-pair-test

do_tgevent-tests: tgevent-test
	./tgevent-test

do_tgexec-tests: tgexec-test
	./tgexec-test

do_tgcollector: tgcollector
	./tgcollector

include $(TOP)/make.common
