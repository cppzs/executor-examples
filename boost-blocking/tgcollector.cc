// test for concentrator based on coroutine executor

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "tgexec.hh"
#include "concentrator.hh"

#include <type_traits>

#include <experimental/execution>

#include <lest.hpp>
#include <vector>
#include <iterator>
#include <algorithm>

namespace execution = std::experimental::execution;

std::vector<int> srcVal[2];
std::vector<int> store;

class ReadDev
{
public:
    ReadDev(int id, int count)
      : mySrcId(id)
      , max(count)
    {
    }

    int operator()()
    {
        if (counter < max)
        {
            ++counter;
            int ret =  (mySrcId * 100) + counter;
            srcVal[mySrcId-1].push_back(ret);
            return ret;
        }
        return 0;
    }

private:
    int mySrcId;
    int counter = 0;
    int max;
};

class StoreData
{
public:
    void operator()(int v)
    {
        store.push_back(v);
    }
};


template<class Ex, class F>
typename std::enable_if_t<std::is_invocable<F>::value, Ex>
  require(Ex ex, F)
{
    return ex;
}

template<class Ex>
auto  require(Ex ex, StoreData)
{
    return ex;
}

namespace
{
const lest::test tgCollectorTests[] =
{
    CASE("basic overall test")
    {
        typedef Concentrator<TPool
                             , int
                             , ReadDev
                             , ReadDev
                             , StoreData
                             , 1> ConcentratorT;

        TPool pool;
        auto xp = pool.get_executor();

        ConcentratorT dataConcentrator(pool
                                       , ReadDev(1, 4)
                                       , ReadDev(2, 3)
                                       , StoreData());
        dataConcentrator.run(xp);

        EXPECT(srcVal[0].size() + srcVal[1].size() == store.size());
        std::vector<int> src1Part;
        std::copy_if(store.begin()
                     , store.end()
                     , std::back_inserter(src1Part)
                     , [] (int val) { return (val >= 100) && (val < 200); });
        EXPECT(srcVal[0] == src1Part);
        std::vector<int> src2Part;
        std::copy_if(store.begin()
                     , store.end()
                     , std::back_inserter(src2Part)
                     , [] (int val) { return (val >= 200) && (val < 300); });
        EXPECT(srcVal[1] == src2Part);
    },
};
}

int main(int argc, char *argv[])
{
    int status = 0;

    status = lest::run(tgCollectorTests, argc, argv, std::cerr);

    std::clog << status << " failed.\n";

    return status;
}
