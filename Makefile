# Copyright (c) 2018 Detlef Vollmann, vollmann engineering gmbh
#
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)


SUBDIRS = \
	boost-blocking \
	queue \
	rt \

all:
	-@for d in $(SUBDIRS) ; do echo "making all in $$d" ; make -C $$d all ; done

tests:
	-@for d in $(SUBDIRS) ; do echo "making tests in $$d" ; make -C $$d tests ; done


clean:
	-@for d in $(SUBDIRS) ; do echo "making clean in $$d" ; make -C $$d clean ; done

