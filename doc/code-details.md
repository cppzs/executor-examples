% Some details on the code



Basic Data Concentrator
-----------------------

One of the examples for concurrent control structures
is a data concentrator.  It consists of three tasks,
two of them reading input (typically from an external device)
and one task to potentially run some filter and store the data
(or write it to another external device).

Based on a queue as proposed in [P0260](https://wg21.link/P0260)
and a latch as proposed in [P0666](https://wg21.link/P0666),
it's not hard to come up with a concentrator:

```c++
template <typename T, typename IN1, typename IN2, typename OUT>
class Concentrator
{
public:
    Concentrator(IN1 producer1, IN2 producer2, OUT consumer, size_t bufSize)
      : p1(producer1)
      , p2(producer2)
      , c(consumer)
      , q(bufSize)
      , inDone(2)
    {
    }

    template <class Ex>
    void run(Ex exec)
    {
        using execution::require;
        auto exOut = require(exec, execution::twoway);
	auto f = exOut.twoway_execute([this] { consumeWrap(); });

        auto exIn1 = require(exec, execution::oneway);
	exIn1.execute([this] { produceWrap<IN1>(p1); });

        auto exIn2 = require(exec, execution::oneway);
	exIn2.execute([this] { produceWrap<IN2>(p2); });

	inDone.wait();
	q.close();
	f.wait();
    }

private:
    template<typename IN>
    void produceWrap(IN prod)
    {
	while (true)
	{
	    T data{prod()};
	    if (!bool(data)) break;
	    q.wait_push(data);
	}
	inDone.arrive();
    }

    void consumeWrap()
    {
	T data;
	while (q.wait_pop(data) != queue_op_status::closed)
	{
	    c(data);
	}
    }

    IN1 p1;
    IN2 p2;
    OUT c;
    buffer_queue<T> q;
    latch inDone;
};
```

(This could be even simpler by just providing a single `run()` function
or at least by giving deduction guides to the constructor.)

To use the concentrator just create one and start it on an executor:

```c++
auto prod1 = [] ()
             {
                 static array<int, 5> data { 1, 3, 5, 7, 9 };
                 static size_t idx = 0;
                 if (idx != 5)
                 {
                     return data[idx++];
                 }
                 return 0;
             };

auto prod2 = [] ()
             {
                 static array<int, 5> data { 4, 2, 4, 2 };
                 static size_t idx = 0;
                 if (idx != 4)
                 {
                     return data[idx++];
                 }
                 return 0;
             };

vector<int> result;
auto consume = [&] (int v)
               {
                   if (v % 2 == 0)
                   {
                       result.push_back(v);
                   }
               };
Concentrator<int
	     , decltype(prod1)
	     , decltype(prod2)
	     , decltype(consume)>
    dataConcentrator(prod1, prod2, consume, 1);

    static_thread_pool pool{5};
    auto exec = pool.executor();
    dataConcentrator.run(exec);
```

So basic concurrency works.


Real-Time
---------

In the real world one of the input devices might be time critical
and the task reading from it should have a higher priority.

For this we need an executor that provides priorities.
This can be done by providing an execution context `RTThreadContext`
with two functions:

```c++
class RTThreadContext
{
public:
    RTExecutor get_executor(int prio);
    template <class F> void spawn(int prio, F &&fun);
};
```

Based on this an actual executor (that only supports oneway) is easy:

```c++
class RTExecutor
{
public:
    RTExecutor(RTThreadContext *context, int priority)
      : ctx(context)
      , prio(priority)
    {
    }

    template <class F>
    void execute(F &&fun) const
    {
        ctx->spawn(prio, std::forward<F>(fun));
    }

private:
    RTThreadContext *ctx;
    int prio;
};
```

But how do we tell our Concentrator that one of the tasks needs
a different executor?
[P0113](https://wg21.link/P0113) (and the Networking TS)
have `wrap()` and `get_associated_executor()` for this.
P0443 doesn't provide it, but the `require()` mechanism can be
used for this.

The idea is to ask each task for its executor and then run it
on this executor:

```c++
    template <class Ex>
    void run(Ex exec)
    {
        // ...
        auto exIn1A = require(exec, p1);
        auto exIn1B = require(exIn1A, execution::oneway);
        exIn1B.execute([this] { produceWrap<IN1>(p1); });
        // ...
    }
```

And we provide a general `require()`:
```c++
template<class Ex, class F>
typename std::enable_if_t<std::is_invocable<F>::value, Ex>
  require(Ex ex, F)
{
    return ex;
}
```

And somebody who wants to wrap a task would provide
a respective overload:
```c++
RTThreadContext RTContext;
template<class Ex>
RTExecutor require(Ex ex, decltype(prod1) &)
{
    return RTContext.get_executor(80);
}
```

This specific mechanism doesn't work anymore in recent versions of P0443
due to changes in the `require` specification.
But it was pointed out that it's still doable.

Whether using `require()` for this is a good idea is a different question.
I would prefer an approach along the lines of
`wrap()`/`get_associated_executor()`,
but as long as such an interface doesn't exist, generic control structures
need to use `require()` to get the executor required by a task.


Boost Blocking
--------------

The data concentrator works fine in a classic concurrent world.
But it breaks down if the executor(s) given to it don't provide
concurrent forward progress.

But one of the goals of the executor proposal is to provide
a common interface for all kind of executors, even if they
provide only weakly parallel forward progress.

And something like a data concentrator should be able to run
in a boost blocking environment.

To try this I implemented an executor `TGExecutor`
that completely relies on boost blocking.
It's based on stackful coroutines and implemented with `boost::context`.

To make the implementation easier `TGExecutor` is based on
an execution context `TPool` and blocking is only defined
inside the same pool or from the outside as entry point.
In any given `TPool` only one task can make progress.
This allows the implementation to identify the current task.

Tasks that are not blocked on aren't executed.
So sometimes tasks are not completed.
The destructor of such a task does not complete it, but it will
call the destructors of the local objects of that task (stack unwinding).

[P0073R2](https://wg21.link/P0073R2) proposed an event class
with a `signal()` and `block()` interface.
`TGExecutor` provides a class `TGEvent` with such function as members.


### Queue

The implementation of the data concentrator is based on a concurrent queue.
So the first task is to implement a concurrent queue based on boost blocking.
But this wouldn't really help.  What we need is a concurrent queue
that can work with boost blocking and can work with "normal"
concurrent threads as well.
Such a queue would be templated on the executors involved
and was outlined in my presentation in Oulu.
My implementation now is simpler as it assumes at least compatible
executors on all sides.

The standard (non-lock-free) implementation of a queue is based
on Condition variables and mutexes.
I decided to keep such an implementation.
For concurrent threads C++ provides mutexes and condition variables.
For thread based executors we want to use these, so for other executors
the interface should be as similar as possible.

My synchronization primitives are a bit more complicated than really
necessary as I tried to avoid dynamic memory allocation.
This is encapsulated in
[../boost-blocking/sync-pair.hh](../boost-blocking/sync-pair.hh).


### Mutex

In a not really concurrent boost blocking world a mutex doesn't
need to provide synchronization but just (possibly) boosting
and still mutual exclusion.

To keep the implementation simple I support only a single task
waiting for a blocked mutex.  But by keeping an internal stack
multiple waiters would be possible as well.

With this a mutex based on a `block()`/`signal()` event
is simple:

```c++
template <class Event>
class SyncMutex
{
public:
    SyncMutex(Event &&e) : evt{std::move(e), 0} {}

    void lock()
    {
        // first check if already locked
        if (evt.hasSignallingTask())
        {
            evt.block();
        }
        // now set our own task as the one that need boosting when blocked
        evt.setSignallingCurrent();
    }
    bool try_lock()
    {
        if (evt.hasSignallingTask())
        {
            return false;
        }
        evt.setSignallingCurrent();
        return true;
    }
    void unlock()
    {
        // if something is waiting, we switch to it
        if (evt.isBlocked())
        {
            evt.signal();
        }
        evt.setSignalling(nullptr);
    }

private:
    Event evt;
};
```

`SyncMutex` provides exactly the same interface a `std::mutex`,
except for construction.
To implement e.g. `setSignallingCurrent()` we need a reference to
the pool we're running on.  So we can't create a `TGEvent`
(and hence a `SyncMutex<TGEvent>`) out of thin air.  We need to get it either
from a `TPool` or a `TGExecutor`.  For my example implementation
I provide a function `getMutex()` in `TPool`
and also directly from a task (see below).


### Condition Variable

Implementing a condition variable in a boost blocking context
based on our TGEvent is generally easy:
in `notify_*()` we just signal the other, and in `wait()` we just
block on the other.
The problematic part here is the "other".
How do we know who's notifying a condition variable and who we need
to boost if we're blocking on a condition variable?
The only way is to tell the condition variable explicitly.

This means that we not only need the standard interface
`wait()`, `notify_one()` and `notify_all()`,
but also a register interface.
In my implementation I provide `attach(SyncHandle<Event> &task)`
and `detach()`.
I'll come to `SyncHandle<Event>` in the next section.
I return a handle from attach to allow for detach.

The rest is straightforward:

```c++
template <class Event>
class SyncCondVar
{
public:
    typedef int HandleT;

    SyncCondVar(Event &&e)
      : evt{std::move(e), 0}
      , size{1}
      , notifiers{evt.hasSignallingTask(), nullptr}
    {
        if (notifiers[0] == nullptr)
        {
            size = 0;
        }
    }

    HandleT attach(SyncHandle<Event> &task)
    {
        assert(size != MaxSize);
        notifiers[size] = task.getTask();
        evt.setSignalling(notifiers[size]);
        return size++;
    }

    // we keep the current size (i.e. don't really support dynamic at/de-taching)
    void detach(HandleT idx)
    {
        auto oldTask = notifiers[idx];
        notifiers[idx] = nullptr;
        if (evt.hasSignallingTask() == oldTask)
        {
            typename Event::TaskPtr newNotifier = nullptr;
            for (auto p: notifiers)
            {
                if (p)
                {
                    newNotifier = p;
                    break;
                }
            }
            evt.setSignalling(newNotifier);
            evt.transfer();
        }
    }

    void notify_one()
    {
        evt.signal();
    }

    void notify_all()
    {
        notify_one();
    }

    template <class Mutex>
    void wait(Mutex &mtx)
    {
        mtx.unlock();
        evt.block();
        mtx.lock();
    }

private:
    Event evt;
    static constexpr int MaxSize = 5;
    int size = 0;
    std::array<Event::TaskPtr, MaxSize> notifiers = {nullptr};
};
```

Again we need a `TGEvent` in the constructor.
And I set a hard limit on attachers to avoid dynamic memory allocation.

One interesting detail is `notify_all()`, which just calls `notify_one()`.
Currently I only support multiple notifiers, but not multiple waiters.
For multiple waiters we wouldn't need another register interface,
but like for mutex a stack that we fill on `block()` calls.


### Handle

As mentioned before a boost blocking condition variable needs
a register interface that registers the notifying task.
We could require that the register function is called from the
task that notifies and rely on the internal knowledge of
the current task.

But this restricts the usability as sometimes the synchronization
mechanism is setup from the outside.
So I decided to return from `twoway_execute` a handle to the task.
This handle can be used to get something like a future to wait on,
a condition variable that's already attached, a mutex (just to
put things together) or to attach to a condition variable.
I didn't call this handle `future` as this would be misleading.

I don't return such a handle from `execute()` as for fire-and-forget
tasks the handle would still be overhead.


### Execution context

Synchronization primitives are probably not executor specific
but specific to the execution context.

So I decided to provide an interface in `TPool` to get mutexes
and condition variables:

```c++
class TPool
{
public:
    // ...
    SyncMutex<TGEvent> getMutex() const;
    SyncCondVar<TGEvent> getEmptyCondVar() const;
};
```
This provides condition variables with no attached tasks.



### Queue again

Based on our mutex and condition variable, we can look what this means
for a queue implementation.

The attach / detach requirements of the condition variable make
a general queue interface where everybody can push or pop
not feasible.  Instead we go with one part of P0260 that proposes
front and back ends.  Our queue implementation requires
a separate end for each pushing or popping task.


#### Queue trunk

The base implementation for the queue is a template on
the executor type, the value type to store and the size.
A better option than the executor type would probably be
the type of the execution context.

And we only have a single executor type parameter even though we
can have different executors pushing or popping to/from the queue.
But the requirement is that the synchronization primitives
can at least cooperate if they are not the same.
So a single executor (or execution context) type is sufficient.
The size parameter is just to avoid dynamic memory allocation.

`TQueueTrunk` doesn't provide any public interface except constructors.
We give two constructors, one just with an execution context:

```c++
    TQueueTrunk(ExContextT ctx)
      : notEmpty{ctx.getEmptyCondVar()}
      , notFull{ctx.getEmptyCondVar()}
      , mtx{ctx.getMutex()}
    {
    }
```
Here we take the condition variables and mutex from the execution context.

The other constructor takes handles for a back and front end.:

```c++
    TQueueTrunk(HandleT &backHandle, HandleT &frontHandle)
      : notEmpty{backHandle.getEmptyCondVar()}
      , notFull{frontHandle.getEmptyCondVar()}
      , mtx{backHandle.getMutex()}
    {
    }
```
Here we take the condition variable from the respective handles
and the mutex just from one of the handles.
But it shouldn't really make a difference from where we take
the condition variables here, as we don't attach to them for the trunk.

The trunk also implements the `close()`, `push()` and `pop()` methods,
but as private, and it declares the end types as friends.

`push()` and `pop()` are essentially the same as for "standard"
queue implementations.
`close()` is different: I require that only the back ends (that push)
can close a queue and that all of them must close it before it's
really considered closed.


#### Queue ends

Queue ends can be created from a queue trunk and a handle for the
task to attach:

```c++
    TQueueFront(TQueueTrunk<ExecT, T, BufSize> *base,
                typename TQueueTrunk<ExecT, T, BufSize>::HandleT &task)
      : trunk{base}
      , condVarMe{trunk->notFull.attach(task)}
    {
    }
```

The ends provide the respective queue interface by just forwarding to the trunk:
```c++
template <class ExecT, typename T, size_t BufSize>
class TQueueFront : public TQueueBase
{
public:
    // ...
    Status pop(T &data)
    {
        return trunk->pop(data);
    }
```

```c++
template <class ExecT, typename T, size_t BufSize>
class TQueueBack : public TQueueBase
{
public:
    // ...
    void close()
    {
        trunk->close();
        // we detach here, as we (should) never notify again
        trunk->notEmpty.detach(condVarMe);
        closed = true;
    }

    Status push(T data)
    {
        return trunk->push(data);
    }
```


More generic concentrator
-------------------------

Based on our queue implementation we can now rewrite our implementation
to work with more kinds of executors.

Our queue takes an executor type as template argument,
but it could as well take an execution context.
For the concentrator I decided to go with execution context
right from the beginning.
And as our queue has a template parameter for the size, we should
have it as well and don't take it anymore as constructor parameter.
We also don't need a latch anymore because closing the queue has changed.

```c++
template <class CtxT, class T, class IN1, class IN2, class OUT, size_t bufSize>
class Concentrator
{
public:
    Concentrator(CtxT &exCtx, IN1 producer1, IN2 producer2, OUT consumer)
      : p1(producer1)
      , p2(producer2)
      , c(consumer)
      , q(exCtx)
    {
    }

    template <class Ex> void run(Ex exec);

private:
    template<class IN> void produceWrap(IN prod, QTailT *qPush);

    void consumeWrap(QHeadT *qPop);

    IN1 p1;
    IN2 p2;
    OUT c;
    TQueueTrunk<ExT, T, bufSize> q;
};
```

`consumeWrap()` and `produceWrap()` now take the queue end to
push or pop and `produceWrap()` now closes the queue instead of
arriving at the latch, otherwise there's no change.


### Creating the queue ends

In the old implementation we simply created a queue at construction time
and started the tasks in `run()` on the executor given.

With our new queue this is more complicated as we need to create queue ends
that depend on the respective tasks.
So we first have to queue the tasks with `twoway_execute()`, take the
handles to create the queue ends, and only then allow the tasks to run.

Everything would be much simpler if `twoway_execute()` would give
a reference to the handle of the created task to the function object
started inside the task, but for now we don't have this.

So now we just use a condition variable to wait for the start signal:

```c++
        typename CtxT::Mutex mtx = exec.context().getMutex();
        typedef std::unique_lock<typename CtxT::Mutex> Lock;
        typename CtxT::CondVar goCond = exec.context().getEmptyCondVar();
        bool go = false;
```

We use a pointer to the queue end that's captured by reference
so we can give it to the lambda before the queue end is actually created:

```c++
        QHeadT *qPullP = nullptr;
        HandleT fCons
            = exOutB.twoway_execute([&, this]() -> void
                                    {
                                        {
                                            Lock l{mtx};
                                            while (!go) goCond.wait(l);
                                        }
                                        consumeWrap(qPullP);
                                    });
        QHeadT qPull{&q, fCons};
        qPullP = &qPull;
```

### Starting

With everything setup, we can give the start signal.
And then we just wait for the consume task to finish.
In a boost blocking world this starts off the tasks,
but in a concurrent world everything was started by the start signal.


Using the concentrator
----------------------

### Old example with RTExecutor

To use the new concentrator with a thread pool as before
doesn't work, as the `static_thread_pool` from the
executor's example implementation doesn't provide the
synchronization primitives we require now.

But providing a wrapper around `static_thread_pool`,
`std::mutex` and `std::condition_variable` is simple.
`attach()` and `detach()` just do nothing and all other functions
just forward to the wrapped object.

Using the concentrator with our thread pool wrapper is essentially
the same as before.  We don't even need changes to our `RTExecutor`
as we instantiate the concentrator with our thread pool wrapper
and mutex and condition variables are semantically the same.


### Concentrator based on TGExecutor

Using the concentrator with our coroutine base `TGExecutor` is
exactly the same as with a thread pool.
The only part that's worth to mention is the fact that we need
to provide overloads for `require` for our function types
as we don't have `get_associated_executor()` and don't provide
such generic overloads in the concentrator.
