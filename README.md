% Examples for concurrent control structures based on executors

The code in this repo tries to implement some executors
based on the proposal for the C++ committee <https://wg21.link/P0443>
and some concurrent control structures on top of them

This code is pure proof of concept.
The implementation is based on a lot of trial and error.
It compiles and passes some tests
but is horribly inconsistent and probably still buggy.

Anybody who still wants to compile the code please see the
comments in [make.common](make.common).

For more explanation on the code itself see
[doc/code-details](doc/code-details.md).
