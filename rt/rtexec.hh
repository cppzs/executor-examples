// rtexec.hh
//  a realtime executor to run tasks in their own thread at a specified priority

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef RTEXEC_HH_SEEN
#define RTEXEC_HH_SEEN
#include <thread>
#include <utility>
#include <experimental/execution>
#include <pthread.h>
#include <vector>
#include <mutex>
#include <future>

class RTExecutor;
class RTThreadContext
{
public:
    RTThreadContext() = default;
    RTThreadContext(RTThreadContext const &) = delete;
    RTThreadContext(RTThreadContext &&) = delete;
    ~RTThreadContext()
    {
        for (auto &t : bag)
        {
            t.join();
        }
    }

    RTExecutor get_executor(int prio);

private:
    friend class RTExecutor;

    template <class F>
    void spawn(int prio, F &&fun)
    {
        std::thread t(std::forward<F>(fun));
	pthread_t pt = t.native_handle();
	sched_param params { prio };
	int policy = (prio == 0) ? SCHED_OTHER : SCHED_FIFO;
	pthread_setschedparam(pt, policy, &params);

        std::unique_lock<std::mutex> lck(mtx);
        bag.push_back(std::move(t));
        ++execCount;
        lastExecPrio = prio;
    }

    std::vector<std::thread> bag;
    std::mutex mtx;

public:
    int execCount = 0;
    int lastExecPrio = -1;
};

class RTExecutor
{
public:
    template <class F>
    void execute(F &&fun) const
    {
        ctx->spawn(prio, std::forward<F>(fun));
    }

    template <class F>
    std::experimental::future<void> twoway_execute(F &&fun) const
    {
        std::experimental::packaged_task<void()> tsk{std::forward<F>(fun)};
        auto ret = tsk.get_future(); // need to get the future before we
                                     // move the task out
        ctx->spawn(prio, std::move(tsk));
        return std::move(ret);
    }

    friend bool operator==(RTExecutor const & a, RTExecutor const & b) noexcept
    {
        return (a.prio == b.prio) && (a.ctx == b.ctx);
    }
    friend bool operator!=(RTExecutor const & a, RTExecutor const & b) noexcept
    {
        return !(a == b);
    }
    auto &context() const noexcept { return *ctx; }

private:
    friend class RTThreadContext;

    RTExecutor(RTThreadContext *context, int priority)
      : ctx(context)
      , prio(priority)
    {
    }

    RTThreadContext *ctx;
    int prio;
};

inline RTExecutor RTThreadContext::get_executor(int prio)
{
    return {this, prio};
}
#endif /* RTEXEC_HH_SEEN */

