// threadPool.hh
//  a wrapper around static_thread_pool with our interface

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef THREADPOOL_HH_SEEN
#define THREADPOOL_HH_SEEN

#include <experimental/thread_pool>
#include <experimental/execution>
#include <condition_variable>
#include <mutex>
#include <memory>


using std::experimental::future;
class MyHandle;

class MyMutex
{
public:
    // we kind of cheat: we make our mutex movable
    MyMutex() : pImpl{new std::mutex()} {}
    MyMutex(std::mutex *pMtx) : pImpl{pMtx} {}

    void lock()
    {
        pImpl->lock();
    }
    bool try_lock()
    {
        return pImpl->try_lock();
    }
    void unlock()
    {
        pImpl->unlock();
    }

private:
    friend class MyCondVar;
    std::unique_ptr<std::mutex> pImpl;
};

class MyCondVar
{
public:
    typedef std::nullptr_t HandleT;

    MyCondVar() = default;

    HandleT attach(MyHandle &) { return {}; }

    void detach(HandleT) {}

    void notify_one()
    {
        impl.notify_one();
    }

    void notify_all()
    {
        impl.notify_all();
    }

    void wait(std::unique_lock<MyMutex> &mtx)
    {
        MyMutex *my = mtx.release();
        std::unique_lock<std::mutex> l{*(my->pImpl), std::adopt_lock};
        impl.wait(l);
        std::unique_lock<MyMutex> l2{*my, std::adopt_lock};
        mtx.swap(l2);
    }

private:
    std::condition_variable impl;
};

class MyWaiter
{
public:
    void wait()
    {
        impl->wait();
    }

private:
    friend class MyHandle;

    MyWaiter(future<void> *fut)
      : impl(fut)
    {}

    future<void> *impl;
};

class MyHandle
{
public:
    MyHandle(future<void> &&f) : impl{std::move(f)} {}
    MyHandle(MyHandle &&rhs) = default;
    ~MyHandle() = default;

    MyHandle& operator=(MyHandle &&) = delete;

    MyWaiter getWaiter() { return &impl; }

    MyMutex getMutex()
    {
        return {};
    }
    MyCondVar getCondVar()
    {
        return {};
    }
    MyCondVar getEmptyCondVar()
    {
        return {};
    }

private:
    future<void> impl;
};

class MyExecutor;

class MyThreadPool
{
public:
    typedef MyCondVar CondVar;
    typedef MyMutex Mutex;
    typedef MyExecutor executor_type;

    MyThreadPool(int numThreads)
      : impl(numThreads)
    {}

    executor_type get_executor() noexcept;

    Mutex getMutex() const { return {}; }

    CondVar getEmptyCondVar() const { return {}; }

private:
    friend class MyExecutor;
    std::experimental::static_thread_pool impl;
};

class MyExecutor
{
    typedef std::experimental::static_thread_pool::executor_type ImplT;
public:
    typedef MyHandle Handle;
    typedef MyCondVar CondVar;
    typedef MyMutex Mutex;

    MyExecutor(MyThreadPool *p) : pool(p) {}

    template <class F>
    void execute(F &&fun)
    {
        pool->impl.executor().execute(std::forward<F>(fun));
    }

    template <class F>
    Handle twoway_execute(F &&fun)
    {
        return pool->impl.executor().twoway_execute(std::forward<F>(fun));
    }

    friend bool operator==(MyExecutor const & a, MyExecutor const & b) noexcept
    {
        return a.pool == b.pool;
    }
    friend bool operator!=(MyExecutor const & a, MyExecutor const & b) noexcept
    {
        return !(a == b);
    }
    MyThreadPool &context() const noexcept { return *pool; }
    MyExecutor require(decltype(std::experimental::execution::twoway) const &) { return *this; }

private:
    MyThreadPool *pool;
};

MyThreadPool::executor_type MyThreadPool::get_executor() noexcept
{
    return {this};
}
#endif /*  THREADPOOL_HH_SEEN */
