// data collector
//  running on a concentrator

// usage: collector <in1> <in2> <out>

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "rtexec.hh"
#include "concentrator.hh"

#include <iostream>
#include <fstream>
#include <exception>
#include <system_error>
#include <cstdint>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <experimental/execution>
#include "threadPool.hh"

using std::ofstream;
using std::endl;
using std::exception;
using std::system_error;
using std::cerr;
namespace execution = std::experimental::execution;

struct InData
{
    uint32_t seq;
    uint8_t noise[4000];
    uint32_t data;
    uint8_t padding[88];
};

struct ShortData
{
    ShortData(uint32_t source, uint32_t counter, uint32_t rawData)
      : src(source)
      , seq(counter)
      , data(rawData)
      , valid(true)
    {
    }
    ShortData()
      : valid(false)
    {
    }

    uint32_t src;
    uint32_t seq;
    uint32_t data;

    explicit operator bool() const
    {
	return valid;
    }

private:
    bool valid;
};

class ReadDev
{
public:
    ReadDev(int id, int inFd)
      : mySrcId(id)
      , fd(inFd)
    {
    }

    ShortData operator()();

private:
    int mySrcId;
    int fd;
    InData data;
};

ShortData ReadDev::operator()()
{
    ssize_t ret = 1;

    ssize_t count = 0;
    uint8_t *pos = reinterpret_cast<uint8_t *>(&data);
    while (count < ssize_t(sizeof(data)) && ret > 0)
    {
	ret = read(fd, pos, sizeof(data) - count);
	if (ret > 0)
	{
	    count += ret;
	    pos += ret;
	}
    }
    if (ret > 0)
    {
	return ShortData{uint32_t(mySrcId), data.seq, data.data};
    }
    if (ret < 0)
    {
	cerr << "Error reading from " << mySrcId << ": " << strerror(errno) << endl;
    }

    return ShortData{};
}

class StoreData
{
public:
    StoreData(ofstream &out)
      : of(out)
    {
    }

    void operator()(ShortData const &);

private:
    ofstream &of;
};

void StoreData::operator()(ShortData const &data)
{
    of << "#New Record\tSource: " << data.src
       << "\tNumber: " << data.seq
       << "\tData: " << data.data << endl;
}

RTThreadContext RTContext;
RTExecutor rtExec80 = RTContext.get_executor(80);
int in1;
auto readDev1 = [] { return ReadDev(1, in1)(); };

template<class Ex>
RTExecutor require(Ex ex, decltype(readDev1) &)
{
    return RTContext.get_executor(80);
}

template<class Ex, class F>
Ex require(Ex ex, F&)
{
    return ex;
}


int
main(int argc, char *argv[])
{
    typedef Concentrator<MyThreadPool
                         , ShortData
	                 , decltype(readDev1)
	                 , ReadDev
                         , StoreData
                         , 10> ConcentratorT;

    in1 = open(argv[1], O_RDONLY);
    if (in1 == -1)
    {
	perror(argv[1]);
	return 1;
    }

    int in2 = open(argv[2], O_RDONLY);
    if (in2 == -1)
    {
	perror(argv[2]);
	close(in1);
	return 2;
    }

    try
    {
	ofstream out;
	out.exceptions(std::ios::failbit | std::ios::badbit);
	out.open(argv[3]);

        MyThreadPool pool{5};
        auto xp = pool.get_executor();

	ConcentratorT dataConcentrator{
	    pool
            , readDev1
	    , ReadDev(2, in2)
	    , StoreData(out)};
	dataConcentrator.run(xp);
    }
    catch (system_error &err) { cerr << err.code().message() << endl; }
    catch (exception &err) { cerr << err.what() << endl; }

    std::clog << "RTCount: " << RTContext.execCount
              << ", Prio: " << RTContext.lastExecPrio << '\n';
	
    return 0;
}
