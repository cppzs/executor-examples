// simple test for RTExecutor

/*
 * Copyright (c) 2014-2018 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "rtexec.hh"

#include <experimental/execution>
#include <cassert>

namespace execution = std::experimental::execution;

static_assert(execution::is_oneway_executor_v<RTExecutor>, "Problem one!");
// we don't have a correct two_way as we always return future<void>:
//static_assert(execution::is_twoway_executor_v<RTExecutor>, "Problem two!");

int main()
{
    RTThreadContext ctx;
    auto ex = ctx.get_executor(80);
    int i = 5;
    ex.execute([&i] () { i = 5; });
    assert(i == 5);
    auto f = ex.twoway_execute([&i] { i = 4; });
    f.wait();
    assert(i == 4);
    return 0;
}
